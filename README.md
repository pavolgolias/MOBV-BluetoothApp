# Bluetooth FEI Navigator
This is the school team project developed at STU FEI in Bratislava. It provides these features:
- Scanning and storing of Bluetooth AC points.
- Detecting the current location of user based on scanning of nearby Bluetooth AC points.
- Simple navigation from one saved Bluetooth AC point (or from your current location) to place of another BluetoothAC point.
- Real time updates of list of Bluetooth AC points (we are using real-time databsae Firebase).