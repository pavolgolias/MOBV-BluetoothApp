package sk.stuba.fei.mobv.feibluetoothscanner.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import sk.stuba.fei.mobv.feibluetoothscanner.R;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public class PositionItemDeleteDialogFragment extends DialogFragment {
    private static final String POSITION_TITLE_DIALOG_KEY = "delete.dialog.position.title";
    private static final String POSITION_ID_DIALOG_KEY = "delete.dialog.position.id";
    private static final String POSITION_INDEX_DIALOG_KEY = "delete.dialog.position.index";

    private PositionItemDeleteListener itemDeleteListener;

    public static PositionItemDeleteDialogFragment newInstance(Position position, int positionIndex) {
        PositionItemDeleteDialogFragment dialogFragment = new PositionItemDeleteDialogFragment();
        Bundle bundle = new Bundle();

        bundle.putString(POSITION_TITLE_DIALOG_KEY, position.getTitle());
        bundle.putString(POSITION_ID_DIALOG_KEY, position.getId());
        bundle.putInt(POSITION_INDEX_DIALOG_KEY, positionIndex);
        dialogFragment.setArguments(bundle);

        return dialogFragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final String positionTitle = getArguments().getString(POSITION_TITLE_DIALOG_KEY);
        final String positionId = getArguments().getString(POSITION_ID_DIALOG_KEY);
        final int positionIndex = getArguments().getInt(POSITION_INDEX_DIALOG_KEY);

        return new AlertDialog.Builder(getActivity())
                .setTitle("Do you want to remove position '" + positionTitle + "'?")
                .setIcon(R.drawable.ic_delete_forever_black_24dp)
                .setPositiveButton("Yes, delete", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        itemDeleteListener.onPositionItemDeletePositiveClick(positionId, positionIndex);
                        dialogInterface.dismiss();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        itemDeleteListener.onPositionItemDeleteNegativeClick();
                        dialogInterface.dismiss();
                    }
                })
                .create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            itemDeleteListener = (PositionItemDeleteListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement PositionItemDeleteListener!");
        }
    }

    public interface PositionItemDeleteListener {
        void onPositionItemDeletePositiveClick(String positionId, int positionIndex);

        void onPositionItemDeleteNegativeClick();
    }
}
