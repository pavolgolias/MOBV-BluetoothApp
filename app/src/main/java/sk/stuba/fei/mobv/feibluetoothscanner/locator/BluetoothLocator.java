package sk.stuba.fei.mobv.feibluetoothscanner.locator;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapper;
import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public class BluetoothLocator {
    private static final String LOG_TAG = "BluetoothLocator";

    private static BluetoothLocator instance;
    private final BluetoothAdapter mBluetoothAdapter;
    private Map<String, BluetoothLocatorListener> listeners;
    private BroadcastReceiver bluetoothBroadcastReceiver;
    private BluetoothBroadcastReceiverListener mListener;
    private Context previousContext;

    private BluetoothLocator() {
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        listeners = new HashMap<>();

        bluetoothBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (mListener != null) {
                    String action = intent.getAction();

                    if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                        BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                        int rssi = intent.getShortExtra(BluetoothDevice.EXTRA_RSSI, Short.MIN_VALUE);
                        mListener.onDeviceFound(new BluetoothAP(device.getAddress(), device.getName(), false, rssi, false));
                    } else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
                        mListener.onBroadcastFinish();
                    }
                }
            }
        };
    }

    public static BluetoothLocator getInstance() {
        if (instance == null) {
            instance = new BluetoothLocator();
        }
        return instance;
    }

    public void addLocatorListener(String tag, BluetoothLocatorListener l) {
        listeners.put(tag, l);
    }

    public void removeLocatorListener(String tag) {
        listeners.remove(tag);
    }

    private void onListAccessPointsForPositionUpdate(List<BluetoothAP> aps) {
        for (BluetoothLocatorListener l : listeners.values()) {
            l.onListAccessPointsForPositionUpdate(new ArrayList<>(aps));
        }
    }

    private void onListAccessPointsForPositionFinish(List<BluetoothAP> aps) {
        for (BluetoothLocatorListener l : listeners.values()) {
            l.onListAccessPointsForPositionFinish(new ArrayList<>(aps));
        }
    }

    private void onDeviceLocationUpdate(Position pos) {
        for (BluetoothLocatorListener l : listeners.values()) {
            l.onDeviceLocationUpdate(pos);
        }
    }

    private void onDeviceLocationFinish(Position pos) {
        for (BluetoothLocatorListener l : listeners.values()) {
            l.onDeviceLocationFinish(pos);
        }
    }

    private void lookForAccessPointsNearby(Context context) {
        if (previousContext != null) {
            previousContext.unregisterReceiver(bluetoothBroadcastReceiver);
        }
        mBluetoothAdapter.cancelDiscovery();
        mBluetoothAdapter.startDiscovery();

        IntentFilter filter = new IntentFilter();
        filter.addAction(BluetoothDevice.ACTION_FOUND);
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
        context.registerReceiver(bluetoothBroadcastReceiver, filter);
        this.previousContext = context;
    }

    public void lookForAccessPointsForPosition(Position position, Context context) {
        if (position != null) {
            final List<BluetoothAP> aps = new ArrayList<>();
            if (position.getBluetoothPoints() != null) {
                for (BluetoothAP ap : position.getBluetoothPoints()) {
                    ap.setStored(true);
                }
                aps.addAll(position.getBluetoothPoints());
            }
            Collections.sort(aps);
            onListAccessPointsForPositionUpdate(aps);
            mListener = new BluetoothBroadcastReceiverListener() {
                @Override
                public void onDeviceFound(BluetoothAP ap) {
                    BluetoothAP foundAP = null;
                    for (BluetoothAP a : aps) {
                        if (ap.equals(a)) {
                            foundAP = a;
                            break;
                        }
                    }

                    if (foundAP == null) {
                        aps.add(ap);
                        Collections.sort(aps);
                        onListAccessPointsForPositionUpdate(aps);
                    } else {
                        foundAP.setRssi(ap.getRssi());
                        Collections.sort(aps);
                        onListAccessPointsForPositionUpdate(aps);
                    }
                }

                @Override
                public void onBroadcastFinish() {
                    onListAccessPointsForPositionFinish(aps);
                }
            };
            lookForAccessPointsNearby(context);
        } else {
            Log.e(LOG_TAG, "Position passed to BluetoothLocator is null!");
        }
    }

    public void locateDevice(Context context) {
        final SortedSet<BluetoothAP> aps = new TreeSet<>(new Comparator<BluetoothAP>() {
            @Override
            public int compare(BluetoothAP one, BluetoothAP two) {
                return Integer.compare(two.getRssi(), one.getRssi());
            }
        });

        mListener = new BluetoothBroadcastReceiverListener() {
            @Override
            public void onDeviceFound(BluetoothAP ap) {
                aps.add(ap);
                SimpleEntry<BluetoothAP, Position> mapping = FirebaseWrapper.getInstance().getPositionToApMappingByAp(aps.first());
                if (mapping != null) {
                    if (mapping.getKey().isMainPoint()) {
                        onDeviceLocationUpdate(mapping.getValue());
                    }
                }
            }

            @Override
            public void onBroadcastFinish() {
                SimpleEntry<BluetoothAP, Position> mapping = null;
                while (!aps.isEmpty()) {
                    mapping = FirebaseWrapper.getInstance().getPositionToApMappingByAp(aps.first());
                    if (mapping == null) {
                        aps.remove(aps.first());
                    } else {
                        break;
                    }
                }
                if (mapping != null && mapping.getKey().isMainPoint()) {
                    onDeviceLocationFinish(mapping.getValue());
                } else {
                    onDeviceLocationFinish(null);
                }
            }
        };
        lookForAccessPointsNearby(context);
    }

    private interface BluetoothBroadcastReceiverListener {
        void onDeviceFound(BluetoothAP ap);

        void onBroadcastFinish();
    }
}
