package sk.stuba.fei.mobv.feibluetoothscanner.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.R;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class NavigationPositionsViewAdapter extends RecyclerView.Adapter<NavigationPositionsViewAdapter.NavigationItemViewHolder> {
    private final List<Position> positions;
    private final NavigationPositionsCallback navigationPositionsCallback;

    public NavigationPositionsViewAdapter(List<Position> positions, NavigationPositionsCallback navigationPositionsCallback) {
        this.positions = positions;
        this.navigationPositionsCallback = navigationPositionsCallback;
    }

    @Override
    public NavigationItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.position_item, parent, false);
        return new NavigationPositionsViewAdapter.NavigationItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NavigationItemViewHolder holder, int position) {
        holder.posAbbreviationTextView.setText(Utils.getPositionAbbreviation(positions.get(position)));
        holder.posNameTextView.setText(positions.get(position).getTitle());
        holder.posDateTextView.setText(positions.get(position).getScanDate());

        final int detailItemPosition = position;
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigationPositionsCallback.handlePositionItemClick(positions.get(detailItemPosition));
            }
        });
    }

    @Override
    public int getItemCount() {
        return positions.size();
    }

    class NavigationItemViewHolder extends RecyclerView.ViewHolder {
        final View view;
        final TextView posAbbreviationTextView;
        final TextView posNameTextView;
        final TextView posDateTextView;

        NavigationItemViewHolder(View itemView) {
            super(itemView);

            this.view = itemView;
            this.posAbbreviationTextView = (TextView) itemView.findViewById(R.id.positionAbbreviation);
            this.posNameTextView = (TextView) itemView.findViewById(R.id.positionName);
            this.posDateTextView = (TextView) itemView.findViewById(R.id.positionUpdateDate);
        }
    }

}
