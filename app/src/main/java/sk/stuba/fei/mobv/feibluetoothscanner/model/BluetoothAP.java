package sk.stuba.fei.mobv.feibluetoothscanner.model;

import android.support.annotation.NonNull;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class BluetoothAP implements Serializable, Comparable<BluetoothAP> {
    private String bssid;
    private String name;
    private boolean mainPoint;
    @Exclude
    private int rssi = Integer.MIN_VALUE;
    @Exclude
    private boolean stored = true;

    public BluetoothAP() {
    }

    public BluetoothAP(String bssid, String name, boolean mainPoint, int rssi, boolean stored) {
        this.bssid = bssid;
        this.name = name;
        this.mainPoint = mainPoint;
        this.rssi = rssi;
        this.stored = stored;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BluetoothAP ap = (BluetoothAP) o;

        return getBssid().equals(ap.getBssid());
    }

    @Override
    public int hashCode() {
        return getBssid().hashCode();
    }

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isMainPoint() {
        return mainPoint;
    }

    public void setMainPoint(boolean mainPoint) {
        this.mainPoint = mainPoint;
    }

    @Exclude
    public int getRssi() {
        return rssi;
    }

    @Exclude
    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    @Exclude
    public boolean isStored() {
        return stored;
    }

    @Exclude
    public void setStored(boolean stored) {
        this.stored = stored;
    }

    @Override
    public String toString() {
        return "BluetoothAP{" +
                "bssid='" + bssid + '\'' +
                ", name='" + name + '\'' +
                ", mainPoint=" + mainPoint +
                '}';
    }

    @Override
    public int compareTo(@NonNull BluetoothAP other) {
        return Integer.compare(other.getRssi(), this.getRssi());
    }
}
