package sk.stuba.fei.mobv.feibluetoothscanner;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import sk.stuba.fei.mobv.feibluetoothscanner.adapters.DirectionPointsViewAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocator;
import sk.stuba.fei.mobv.feibluetoothscanner.locator.BluetoothLocatorListenerAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.model.DirectionPoint;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class DirectionsActivity extends AppCompatActivity {

    private static final String LISTENER_TAG = "DirectionsActivity";
    private static final String START_LOCATION = "StartLocation";
    private static final String END_LOCATION = "EndLocation";
    private Toast toast;
    private BluetoothLocator locator;
    private ArrayList<DirectionPoint> directionsPointsList;
    private ProgressBar progressBar;
    private ImageView positionIcon;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

        Position startLocation = (Position) getIntent().getSerializableExtra(START_LOCATION);
        Position endLocation = (Position) getIntent().getSerializableExtra(END_LOCATION);

        TextView directionsInfoText = (TextView) findViewById(R.id.directionsInfoText);
        directionsInfoText.setText(startLocation.getTitle() + " -> " + endLocation.getTitle());

        directionsPointsList = createDirectionsPoints(startLocation, endLocation);
        //Log.i(LISTENER_TAG, directionsPointsList.toString());

        ListView directionsPointsListView = (ListView) findViewById(R.id.directionsPointsList);
        DirectionPointsViewAdapter directionsPointsAdapter = new DirectionPointsViewAdapter(getApplicationContext(), directionsPointsList);
        directionsPointsListView.setAdapter(directionsPointsAdapter);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        positionIcon = (ImageView) findViewById(R.id.positionIcon);

        instantiateBluetoothLocator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        positionIcon = (ImageView) findViewById(R.id.positionIcon);
        locator.locateDevice(getApplicationContext());
        positionIcon.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        Utils.showToastMessage(toast, "Locating device!", Toast.LENGTH_SHORT);
    }

    private ArrayList<DirectionPoint> createDirectionsPoints(Position startLocation, Position endLocation) {
        directionsPointsList = new ArrayList<>();

        if (comparePositionBuildings(startLocation, endLocation) == 0) {
            //positions are on the same block
            DirectionPoint directionPointStart = new DirectionPoint();
            directionPointStart.setBuildingDescription(createTitleMessage(startLocation));
            directionPointStart.setTextDescription(createUseElevatorToFloorMessage(endLocation));
            directionsPointsList.add(directionPointStart);

            DirectionPoint directionPointEnd = new DirectionPoint();
            directionPointEnd.setBuildingDescription(createBuildingMessage(endLocation));
            directionPointEnd.setTextDescription(createYouAreAtYourDestinationMessage(endLocation));
            directionsPointsList.add(directionPointEnd);
        } else {
            //positions are not on the same block;
            if (isPositionOnBasementFloor(startLocation)) {
                DirectionPoint directionPointStart1 = new DirectionPoint();
                directionPointStart1.setBuildingDescription(createBuildingMessage(startLocation));
                directionPointStart1.setTextDescription(createContinueToBasementBuildingMessage(endLocation));
                directionsPointsList.add(directionPointStart1);
            } else {
                DirectionPoint directionPointStart2 = new DirectionPoint();
                directionPointStart2.setBuildingDescription(createTitleMessage(startLocation));
                directionPointStart2.setTextDescription(createUseElevatorToBasementFloorMessage(startLocation));
                directionsPointsList.add(directionPointStart2);
            }

            DirectionPoint directionPointNext = new DirectionPoint();
            directionPointNext.setBuildingDescription(createBuildingMessage(startLocation));
            directionPointNext.setTextDescription(createContinueToBasementBuildingMessage(endLocation));
            directionsPointsList.add(directionPointNext);

            if (isPositionOnBasementFloor(endLocation)) {
                DirectionPoint directionPointEnd1 = new DirectionPoint();
                directionPointEnd1.setBuildingDescription(createTitleMessage(endLocation));
                directionPointEnd1.setTextDescription(createYouAreAtYourDestinationMessage(endLocation));
                directionsPointsList.add(directionPointEnd1);
            } else {
                DirectionPoint directionPointEnd2 = new DirectionPoint();
                directionPointEnd2.setBuildingDescription(createBuildingMessage(endLocation));
                directionPointEnd2.setTextDescription(createUseElevatorToFloorMessage(endLocation));
                directionsPointsList.add(directionPointEnd2);

                DirectionPoint directionPointEnd3 = new DirectionPoint();
                directionPointEnd3.setBuildingDescription(createTitleMessage(endLocation));
                directionPointEnd3.setTextDescription(createYouAreAtYourDestinationMessage(endLocation));
                directionsPointsList.add(directionPointEnd3);
            }
        }
        return directionsPointsList;
    }

    private void instantiateBluetoothLocator() {
        locator = BluetoothLocator.getInstance();
        final TextView positionDescTextView = (TextView) findViewById(R.id.positionDesc);
        final TextView positionStatusTextView = (TextView) findViewById(R.id.positionStatus);
        locator.addLocatorListener(LISTENER_TAG, new BluetoothLocatorListenerAdapter() {
            @Override
            public void onDeviceLocationUpdate(Position likelyPosition) {
                positionDescTextView.setText(Utils.getPositionAbbreviation(likelyPosition) + " - " + likelyPosition.getTitle());
            }

            @Override
            public void onDeviceLocationFinish(Position position) {
                if (position == null) {
                    positionDescTextView.setText(getString(R.string.positionNotFound));
                    positionStatusTextView.setText(getString(R.string.actualPositionStatusNotFound));
                } else {
                    positionDescTextView.setText(Utils.getPositionAbbreviation(position) + " - " + position.getTitle());
                    positionStatusTextView.setText(getString(R.string.actualPositionStatusFound));
                }
                positionIcon.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                Utils.showToastMessage(toast, "Device localization ended!", Toast.LENGTH_SHORT);
            }
        });

        LinearLayout currentPosition = (LinearLayout) findViewById(R.id.currentPosition);

        currentPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locator = BluetoothLocator.getInstance();
                final TextView positionDescTextView = (TextView) findViewById(R.id.positionDesc);
                final TextView positionStatusTextView = (TextView) findViewById(R.id.positionStatus);
                locator.addLocatorListener(LISTENER_TAG, new BluetoothLocatorListenerAdapter() {
                    @Override
                    public void onDeviceLocationUpdate(Position likelyPosition) {
                        positionDescTextView.setText(Utils.getPositionAbbreviation(likelyPosition) + " - " + likelyPosition.getTitle());
                    }

                    @Override
                    public void onDeviceLocationFinish(Position position) {
                        if (position == null) {
                            positionDescTextView.setText(getString(R.string.positionNotFound));
                            positionStatusTextView.setText(getString(R.string.actualPositionStatusNotFound));
                        } else {
                            positionDescTextView.setText(Utils.getPositionAbbreviation(position) + " - " + position.getTitle());
                            positionStatusTextView.setText(getString(R.string.actualPositionStatusFound));
                        }
                        positionIcon.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        Utils.showToastMessage(toast, "Device localization ended!", Toast.LENGTH_SHORT);
                    }
                });
                locator.locateDevice(getApplicationContext());
                positionIcon.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                Utils.showToastMessage(toast, "Locating device!", Toast.LENGTH_SHORT);
            }
        });
    }

    private Integer comparePositionBuildings(Position startLocation, Position endLocation) {
        if (startLocation.getBuilding().equals(endLocation.getBuilding())) {
            return 0; //positions are on the same floor
        }
        return startLocation.getBuilding().compareTo(endLocation.getBuilding());
    }

    private boolean isPositionOnBasementFloor(Position position) {
        return position.getFloor() == 0;
    }

    private String createBuildingMessage(Position position) {
        return getResources().getString(R.string.directionPointBuildingName) + position.getBuilding();
    }

    private String createTitleMessage(Position position) {
        return getResources().getString(R.string.directionPointBuildingName) +
                Utils.getPositionAbbreviation(position);
    }

    private String createUseElevatorToFloorMessage(Position position) {
        return getResources().getString(R.string.directionPointUseFloor) + String.valueOf(position.getFloor());
    }

    private String createUseElevatorToBasementFloorMessage(Position position) {
        return getResources().getString(R.string.directionPointUseFloorToBasement) + position.getBuilding();
    }

    private String createYouAreAtYourDestinationMessage(Position position) {
        return getResources().getString(R.string.directionPointYouAreAtDestination) + String.valueOf(position.getFloor());
    }

    private String createContinueToBasementBuildingMessage(Position position) {
        return getResources().getString(R.string.directionPointContinueThroughBasement) + position.getBuilding();
    }

}
