package sk.stuba.fei.mobv.feibluetoothscanner;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Date;

import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class AddPositionActivity extends AppCompatActivity {
    private Toast toast;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_position);

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Button nextButton = (Button) findViewById(R.id.add_position_next_button);
        final EditText positionNameEditText = (EditText) findViewById(R.id.positionName);
        final EditText buildungNameEditText = (EditText) findViewById(R.id.buildingName);
        final Spinner floorSpinner = (Spinner) findViewById(R.id.spinnerFloor);

        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int floor = Integer.parseInt(floorSpinner.getSelectedItem().toString());
                String title = positionNameEditText.getText().toString();
                String building = buildungNameEditText.getText().toString();
                String scanDate = Utils.DATE_FORMAT.format(new Date());

                if (title.isEmpty() || building.isEmpty()) {
                    Utils.showToastMessage(toast, "Fill in all data!", Toast.LENGTH_SHORT);
                    return;
                }

                Position position = new Position();
                position.setTitle(title);
                position.setFloor(floor);
                position.setBuilding(building);
                position.setScanDate(scanDate);
                position.setStored(false);
                position.setBluetoothPoints(new ArrayList<BluetoothAP>());

                Intent intent = new Intent(getApplicationContext(), PositionAccessPointsActivity.class);
                intent.putExtra(DashboardActivity.SERIALIZABLE_KEY_POSITION, position);
                startActivity(intent);
                finish();
            }
        });
    }
}
