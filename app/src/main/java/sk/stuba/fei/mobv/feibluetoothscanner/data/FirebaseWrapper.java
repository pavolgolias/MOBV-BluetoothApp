package sk.stuba.fei.mobv.feibluetoothscanner.data;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import sk.stuba.fei.mobv.feibluetoothscanner.model.BluetoothAP;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;

public class FirebaseWrapper {
    private static final String LOG_TAG = "FirebaseWrapper";

    private static FirebaseWrapper instance;
    private DatabaseReference reference;
    private Map<String, FirebaseWrapperListener> listeners;
    private List<Position> positions;
    private Map<BluetoothAP, Position> positionsMap;

    private FirebaseWrapper() {
        listeners = new HashMap<>();
        positions = new ArrayList<>();
        positionsMap = new HashMap<>();

        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
        reference = FirebaseDatabase.getInstance().getReference();
        reference.keepSynced(true);
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Position> positionsMap = dataSnapshot.getValue(new GenericTypeIndicator<HashMap<String, Position>>() {
                });

                if (positionsMap != null) {
                    FirebaseWrapper.this.positions = new ArrayList<>(positionsMap.values());
                    Collections.sort(FirebaseWrapper.this.positions);
                    FirebaseWrapper.this.positionsMap = createReverseMap(FirebaseWrapper.this.positions);
                    FirebaseWrapper.this.onDataChange(FirebaseWrapper.this.positions);
                } else {
                    Log.e(LOG_TAG, "Downloaded data are NULL!");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG, "Error occured while downloading data from firebase.");
            }
        });
        reference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Map<String, Position> positionsMap = dataSnapshot.getValue(new GenericTypeIndicator<HashMap<String, Position>>() {
                });

                if (positionsMap != null) {
                    FirebaseWrapper.this.positions = new ArrayList<>(positionsMap.values());
                    Collections.sort(FirebaseWrapper.this.positions);
                    FirebaseWrapper.this.positionsMap = createReverseMap(FirebaseWrapper.this.positions);
                    FirebaseWrapper.this.onInitialDataLoaded(FirebaseWrapper.this.positions);
                } else {
                    Log.e(LOG_TAG, "Downloaded data are NULL!");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(LOG_TAG, "Error occured while downloading data from firebase.");
            }
        });
    }

    private static Map<BluetoothAP, Position> createReverseMap(@NonNull final List<Position> positions) {
        Map<BluetoothAP, Position> map = new HashMap<>();
        for (Position p : positions) {
            if (p.getBluetoothPoints() != null) {
                for (BluetoothAP ap : p.getBluetoothPoints()) {
                    if (map.put(ap, p) != null) {
                        Log.e(LOG_TAG, String.format("One bluetooth AP=[%s] is mapped to multiple positions!", ap.getBssid()));
                    }
                }
            }
        }
        return map;
    }

    public static FirebaseWrapper getInstance() {
        if (instance == null) {
            instance = new FirebaseWrapper();
        }
        return instance;
    }

    public void addListener(String tag, FirebaseWrapperListener l) {
        listeners.put(tag, l);
    }

    public void removeListener(String tag) {
        listeners.remove(tag);
    }

    public void removePosition(String positionId) {
        reference.child(positionId).setValue(null);
    }

    public void createNewPosition(Position position) {
        position.setId(reference.push().getKey());
        updateExistingPosition(position);
    }

    public void updateExistingPosition(Position position) {
        reference.child(position.getId()).setValue(position);
    }

    public List<Position> getPositions() {
        return positions;
    }

    public Position getPositionByAP(BluetoothAP ap) {
        return positionsMap.get(ap);
    }

    public SimpleEntry<BluetoothAP, Position> getPositionToApMappingByAp(BluetoothAP ap) {
        Position pos = positionsMap.get(ap);
        BluetoothAP keyAp = null;
        if (pos != null) {
            for (BluetoothAP bap : pos.getBluetoothPoints()) {
                if (bap.equals(ap)) {
                    keyAp = bap;
                    break;
                }
            }
            return new SimpleEntry<>(keyAp, pos);
        }
        return null;
    }

    private void onDataChange(final List<Position> positions) {
        for (FirebaseWrapperListener l : listeners.values()) {
            l.onDataChange(new ArrayList<>(positions));
        }
    }

    private void onInitialDataLoaded(final List<Position> positions) {
        for (FirebaseWrapperListener l : listeners.values()) {
            l.onInitialDataLoaded(new ArrayList<>(positions));
        }
    }
}
