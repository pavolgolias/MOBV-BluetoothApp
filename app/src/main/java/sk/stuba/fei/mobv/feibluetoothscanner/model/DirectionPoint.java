package sk.stuba.fei.mobv.feibluetoothscanner.model;

public class DirectionPoint {

    private String buildingDescription;
    private String textDescription;

    public DirectionPoint() {
    }

    public DirectionPoint(String buildingDescription, String textDescription) {
        this.buildingDescription = buildingDescription;
        this.textDescription = textDescription;
    }

    public String getBuildingDescription() {
        return buildingDescription;
    }

    public void setBuildingDescription(String buildingDescription) {
        this.buildingDescription = buildingDescription;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public void setTextDescription(String textDescription) {
        this.textDescription = textDescription;
    }

    @Override
    public String toString() {
        return "\nDirectionPoint{" +
                "buildingDescription='" + buildingDescription + '\'' +
                ", textDescription='" + textDescription + '\'' +
                '}';
    }
}
