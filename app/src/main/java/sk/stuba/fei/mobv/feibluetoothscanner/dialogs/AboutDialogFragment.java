package sk.stuba.fei.mobv.feibluetoothscanner.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import sk.stuba.fei.mobv.feibluetoothscanner.R;

public class AboutDialogFragment extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new AlertDialog.Builder(getActivity())
                .setTitle(R.string.about_title)
                .setMessage("Created by:\n\nPavol Goliáš\nAndrej Kosár\nJakub Kertýs\nTomáš Tarasovič\nAlexandra Biľanská")
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
                .create();
    }
}