package sk.stuba.fei.mobv.feibluetoothscanner;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sk.stuba.fei.mobv.feibluetoothscanner.adapters.NavigationPositionsCallback;
import sk.stuba.fei.mobv.feibluetoothscanner.adapters.NavigationPositionsViewAdapter;
import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapper;
import sk.stuba.fei.mobv.feibluetoothscanner.data.FirebaseWrapperListener;
import sk.stuba.fei.mobv.feibluetoothscanner.dividers.DividerItemDecoration;
import sk.stuba.fei.mobv.feibluetoothscanner.model.Position;
import sk.stuba.fei.mobv.feibluetoothscanner.utils.Utils;

public class NavigationActivity extends AppCompatActivity implements FirebaseWrapperListener, NavigationPositionsCallback {

    private static final String NAV_LISTENER_TAG = "NavigationActivity";
    private static final String START_LOCATION_KEY = "StartLocation";
    private static final String END_LOCATION_KEY = "EndLocation";
    private Toast toast;
    private EditText startLocation;
    private EditText endLocation;
    private Position startPositionLocation;
    private Position endPositionLocation;
    private RecyclerView navigationRecyclerView;
    private List<Position> positions;
    //when the activity is created, the first edit text, i.e startLocation is selected
    private boolean selectedStartLocationEditText = true;

    @SuppressLint("ShowToast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);

        toast = Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        startLocation = (EditText) findViewById(R.id.startLocation);
        startLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                selectedStartLocationEditText = true;
                navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(positions, NavigationActivity.this));
                return false;
            }
        });
        startLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                handleSearchExecution(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        endLocation = (EditText) findViewById(R.id.endLocation);
        endLocation.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                selectedStartLocationEditText = false;
                navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(positions, NavigationActivity.this));
                return false;
            }
        });
        endLocation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                handleSearchExecution(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        //getting data from firebase
        FirebaseWrapper firebaseWrapper = FirebaseWrapper.getInstance();
        firebaseWrapper.addListener(NAV_LISTENER_TAG, this);
        positions = firebaseWrapper.getPositions();

        navigationRecyclerView = (RecyclerView) findViewById(R.id.navigationPositionsRecyclerView);
        navigationRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        navigationRecyclerView.setItemAnimator(new DefaultItemAnimator());
        navigationRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext()));
        navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(positions, this));

        FloatingActionButton fabNavigation = (FloatingActionButton) findViewById(R.id.fabNavigationButton);
        fabNavigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //check for inputs
                if (startLocation.getText().toString().isEmpty()) {
                    Utils.showToastMessage(toast, "Start location input is empty!", Toast.LENGTH_SHORT);
                } else if (endLocation.getText().toString().isEmpty()) {
                    Utils.showToastMessage(toast, "End location input is empty!", Toast.LENGTH_SHORT);
                } else {
                    if (startPositionLocation == null || endPositionLocation == null) {
                        Utils.showToastMessage(toast, "One of inputs is not a selected position!", Toast.LENGTH_SHORT);
                    } else if (startPositionLocation.equals(endPositionLocation)) {
                        Utils.showToastMessage(toast, "Cannot navigate if inputs are identical!", Toast.LENGTH_SHORT);
                    } else {
                        Intent intent = new Intent(getApplicationContext(), DirectionsActivity.class);
                        intent.putExtra(START_LOCATION_KEY, startPositionLocation);
                        intent.putExtra(END_LOCATION_KEY, endPositionLocation);
                        startActivity(intent);
                    }
                }
            }
        });
    }

    private void handleSearchExecution(String text) {
        if (text == null || text.isEmpty()) {
            navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(positions, this));
        } else {
            List<Position> matchedPositions = new ArrayList<>();
            for (Position position : positions) {
                if (position.getBuilding().toLowerCase().contains(text.toLowerCase())
                        || position.getTitle().toLowerCase().contains(text.toLowerCase())) {
                    matchedPositions.add(position);
                }
            }
            navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(matchedPositions, this));
        }
    }

    @Override
    public void onDataChange(List<Position> positions) {
        this.positions = positions;
        navigationRecyclerView.setAdapter(new NavigationPositionsViewAdapter(this.positions, this));
    }

    @Override
    public void onInitialDataLoaded(List<Position> positions) {
        //do nothing
    }

    @Override
    public void handlePositionItemClick(Position position) {
        //filling the selected edit text with the title of the position
        if (selectedStartLocationEditText) {
            startPositionLocation = position;
            startLocation.setText(startPositionLocation.getTitle());
        } else {
            endPositionLocation = position;
            endLocation.setText(endPositionLocation.getTitle());
        }
    }
}
